# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Hadrien Boulanger|
| **Scrum Master**        | Amin Elanbari                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

- Nous n'avons pas pu faire les user stories que nous avions définies car nous avons passé ce sprint à tester symfony, que nous ne conaissions pas encore pour la majorité du groupe.
- Nous avons changé d'ordinateur au dernier moment ce qui a entrainé des problèmes techniques pour la présentation.

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
3 sur 7

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
1
## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

- nous n'avons pratiquement pas utilisé Gitlab. 
- les difficultés à maîtriser un nouvel outil ont été importantes.
- toute l'équipe ne maîtrise pas symfony.
- la mise en place du discord a facilité la communication.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

- avoir le projet sur chacun de nos ordinateurs personnels.
- utiliser Gitlab le plus possible pour fluidifier le travail d'équipe.
- passer de symfony à du php objet simple : pas de framework php, identifier les éléments à récupérer de l'ancien projet, identifier la structure du projet Git
- se connecter au moins 2 fois par jour sur Discord pour se tenir au courant de l'avancement
- (avant lundi prochain) tous faire le cours open classroom sur le php objet.
- se regrouper jeudi prochain tous ensemble pour travailler le projet.



### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

Le SM s'assurera dorénavant que la méthode agile est bien respectée (commits fréquents etc...).
## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

La vitesse de passage de symfony à du simple php objet (recréation de ce qui a déja été fait et ajout des nouvelles stories en accord avec ce choix).
### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

- US1 : en tant que membre je peux ajouter un objet a prêter.
- US2 : en tant que membre je peux écrire et modifier les spécificité d'un objet que je souhaite prêter.
- US3 : en tant que membre je peux rechercher un objet.
- US4 : en tant que membre je peux obtenir les infos du propriétaire de l'objet.
- US5 : en tant que membre je peux obtenir les spécificités d'un objet disponible au prêt.

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

Pas de nouvelles US car il faut déjà finir les autres. 


## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *1* 	    |  *3* 	|  *1* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *1* 	    |  *2* 	|  *2* 	|

 
 