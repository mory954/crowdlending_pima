# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Hadrien Boulanger|
| **Scrum Master**        | Etienne Biville                   |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

- Nous n'avons pas pu faire les user stories que nous avions définies car nous avons passé ce sprint à nous former à symfony, que nous ne conaissions pas encore pour la majorité du groupe.
- Nous avons changé d'ordinateur au dernier moment ce qui a entrainé des problèmes techniques pour la présentation.

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

Nous n'avons donc pas fait les user stories prévues mais nous avons quand même fait les pages d'inscription et de connexion pour notre site. Ce n'était pas la chose prioritaire à faire selon la méthode agile mais cela nous a permis de nous former plus facilement à symfony.

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

## Rétrospective de l'itération précédente
  
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

- manque de communication lors du sprint : nous utilisions Whatsapp qui n'est pas adapté.
- nous n'avons pratiquement pas utilisé Gitlab et ne nous sommes échangé les fichiers que par clé usb et email. 
- les difficultés à maîtriser un nouvel outil ont été importantes.
- notre présentation était baclée et peu adapté à la présentation devant un client.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

- mise en place d'un slack dédié au projet pour faciliter la communication.
- utiliser Gitlab le plus possible pour fluidifier le travail d'équipe.
- soigner la présentation, arriver préparé en condition de rdv avec un client.
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

rôles pas définis la fois précédente (groupe de 4) mais maintenant rôles bien définis car nous sommes 5 : répartition des tâches plus en adéquation avec la distribution des rôles.

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
Surtout les difficultés techniques concernant la prise en main d'outils qui sont nouveaux pour la plupart d'entre nous. 
### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Répondre ici

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

- US1 : en tant que membre je peux ajouter un objet a prêter.
- US2 : en tant que membre je peux écrire et modifier les spécificité d'un objet que je souhaite prêter.
- US3 : en tant que membre je peux rechercher un objet.
- US4 : en tant que membre je peux obtenir les infos du propriétaire de l'objet.
- US5 : en tant que membre je peux obtenir les spécificités d'un objet disponible au prêt.
- Tech Story 1 : Créer base de données objets
- Tech Story 2 : Créer base de données utilisateurs

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *1* 	    |  *3* 	|  *1* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *2* 	|  *3* 	|

 
 