# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Hadrien Boulanger        |
| **Scrum Master**        | Nour Ajabboune           |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

-Notre réunion le jeudi nous a permis de faire des avancées significatives

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
-3 US ont été terminées lors de ce sprintv 

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

- US1 : en tant que membre je peux ajouter un objet a prêter.
- US2 : en tant que membre je peux écrire les spécificité d'un objet que je souhaite prêter.
- US5 : en tant que membre je peux obtenir les spécificités d'un objet disponible au prêt.


## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

-Nous sommes rassurés de voir que nous pouvions travailler en équipe de manière efficace. Nous nous sommes réunis à mi-sprint pour faire un état des lieux de l'avancement de chacun et pour finaliser nos composants. Nous avons pris la décision de repasser sur Symfony une bonne fois pour toute. Cela nous a réussi.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

- se réunir à nouveau le jeudi 13/11 pour mettre en commun nos travaux


### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
Le PO doit être très attentif puisqu'il s'agit du dernier sprint.

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

L'évènement majeur est l'arrivée des partiels la semaine du dernier sprint.
### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

- US1 : en tant que membre je peux rechercher un objet.
- US2 : en tant que membre je peux obtenir les infos du propriétaire de l'objet.

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

- US3 : en tant que membre je peux emprunter un objet. Lorsque l'objet est emprunté, il a le statut "emprunté" et n'est plus "disponible".
- US4 : en tant que membre je peux rendre un objet à la date limite du prêt ou bien avant
- US5 : en tant que membre propriétaire je peux supprimer les articles que j'ai ajouté
- US6 : en tant que membre propriétaire je peux modifier les articles que j'ai ajouté (infos modifiables : photo, description.  Mais pas si l'objet est prêté ou non) 

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *1* 	|  *4* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *1* 	    |  *2* 	|  *2* 	|

 