# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Hadrien Boulanger|
| **Scrum Master**        | Amin Elambari|

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
 Première itération donc RAS 


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
Première itération


### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Répondre ici

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

On a crée des user story que l'on va se répartir pour le prochain sprint.
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Répondre ici

Première itération

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Répondre ici

-Des difficultés techniques sont à prévoir au vu de notre niveau moyen en programmation web

-Des difficultés peut être aussi pour la première implémentation du site

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Répondre ici



### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Répondre ici


US 1: -en tant que: membre
      -je peux: rechercher un objet
      -pour: trouver l'objet dont j'ai besoin 
      
US 1: -en tant que: membre
      -je peux: ajouter un objet que je peux prêter 
      -pour: mettre à disposition les objets que je n'utilise plus
      
US 1: -en tant que: membre
      -je peux: obtenir les informations sur le proprietaire de l'objet
      -pour: savoir à qui je m'adresse
      
US 1: -en tant que: membre
      -je peux: obtenir les informations sur les spécificités de l'objet
      -pour: être sur que l'objet correspond à mes besoins
      
US 1: -en tant que: membre
      -je peux: derouler la liste des resultats de ma recherche
      -pour: voir plusieurs objets correspondant à mes besoins




## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *2* 	    |  *2* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *2* 	|  *2* 	|

 
 